# Installation

## Pipeline outline
https://docs.google.com/drawings/d/1_xANRcLMJA554vyaOAES6_r_NjXMZp6mMKmIQL_pkRs/edit?usp=sharing

## From source

### Obtain a copy from Kosinski Lab

### For authorized EMBL users:
```
git clone git@git.embl.de:grp-kosinski/efitter.git
```

## Using conda

TODO

## Using pip

TODO

## Install dependencies

### UCSF Chimera

* On Heidelberg cluster just use:

    ```
    module load Chimera
    ```

### R

* On Heidelberg cluster just use:

    ```
    module load R
    ```

* Otherwise, install R from
* Install the following R packages:
    * fdrtool
    * psych

### python2.7

### bash

Currently the pieline runs only on Linux with bash installed (usually bash is installed by default on Linux)

### pdb_utils
```
git clone git@git.embl.de:grp-kosinski/pdb_utils.git
```

# Usage

## Running the fitting

1. If you installed from source, add the source pacakges to your PYTHONPATH

    ```
    export PYTHONPATH=<your path>/efitter:$PYTHONPATH
    export PYTHONPATH=<your path>/pdb_utils:$PYTHONPATH
    ```

1. Create a new directory e.g. `fitting`
1. Create a directory for the PDB structures you want to fit, e.g. `fitting/PDBs`
1. Copy `params_template_cluster.py` or `params_template_multicore.py` from `efitter/doc` directory:

    ```
    cp <path_to_efitter>/efitter/doc/params_template_cluster.py fitting/params.py
    ```
    * `params_template_cluster.py` - template for submissions on a computer cluster (with SLURM as an example)
    * `params_template_multicore.py`- template for running on a local Linux computer

1. Edit the paths and parameters in `params.py`
1. Run the fitting:

    ```
    python <path_to_efitter>/efitter/scripts/fit.py params.py
    ```

1. If any jobs have not completed, investigate their respective logs, and re-run using `run_crashed_only` parameter in `params.py`.

## Calculate additional fitting scores with TEMPy [EXPERIMENTAL]
Add various scores to the output files using TEMPy library. See the following publications for explanations of the scores:

http://journals.iucr.org/j/issues/2015/04/00/vg5014/index.html

https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5479444/

1. Download TEMPy from: http://tempy.ismb.lon.ac.uk/index.html

1. Install TEMPy in a separate Python 2.7 environment and its dependencies
    ```
    module load Anaconda3
    conda create -n tempy1.1 python=2.7 
    source activate tempy1.1
    conda install numpy
    conda install -c anaconda biopython
    pip install 'scipy==0.18.0'
    cd /g/kosinski/kosinski/software/TEMPy-1.1/
    python setup.py install

    ```

1. Set up the environment
    ```
    module load Chimera
    module load Anaconda3
    source activate tempy1.1
    export PYTHONPATH=<your path>/pdb_utils:$PYTHONPATH
    ```

1. Run
    ```
    python <path_to_efitter>/efitter/scripts/score_with_TEMPy <solutions.csv> <path_to_em_map.mrc> <em_map_threshold> <ori_pdb_filename> <model_map_resolution> <model_map_threshold> <a_temp_directory_path> <outfilename.csv>
    ``` 
* The ```<a_temp_directory_path>``` should be sth in /scratch, the not to clever script will be saving molmaps there, high IO! TODO: change back to /tmp/ ?
* To adjust ```<model_map_threshold>``` option, generate molmap in chimera for your rigid bodies and adjust the threshold so it encompasses the structure
* Depending on the number of fits, it can run for several hours.

## Analysis

In the output, you should have got the following directory structure:

```
master_outdir/ # Directory specified with "master_outdir" parameter in params.py
    config_prefix1/ # named after config_prefix specifications in fitmap_args in params.py
        map1.mrc/   # named after the filenames of the EM maps used
            map1.mrc  # symbolic link to the reference map
            pdb_file_name1.pdb/     # named after the pdb file names used for fitting
                solutions.csv   # the list of solutions and their scores
                log_err.txt     # standard error log
                log_out.txt     # standeard output log
                run.sh          # sbatch script used for running the job
                ori_pdb.pdb     # symbolic link to the original query file
                map1.mrc  # symbolic link to the reference map
            pdb_file_name2.pdb/
            pdb_file_name3.pdb/
            config.txt          # A config file for fitting, saved FYI.
        map2.mrc/
    config_prefix2/
    config_prefix3/
```

### Statistical analysis

#### Method 1

 1. Using `genpval` script

    ```
    genpval master_outdir/
    ```

#### Method 2

A bash command like this would iterate through all output directories and run the script there:

```
cd master_outdir/
parent_dir=$PWD
for f in `ls fits_chimera/fit_cam_inside0.3_fa_10000/tRNA_postprocessed_class1lobeFSCcl3dr2weightedtRNAsubstract_mask1bf40.mrc/*.pdb | grep ":" | perl -p -e "s/\
://"`;
    do
        struct=`basename $f`;
        echo $struct
        cd $f;
        Rscript /g/kosinski/kosinski/devel/efitter/scripts/pval_from_solutions.R solutions.csv cam_score
        python /g/kosinski/kosinski/devel/efitter/scripts/draw_solutions_histogram.py solutions.csv histogram.png
        cd $parent_dir
    done
```
#### Method 3 ("Manually")

1. Enter the directory for the given run:

    ```
    cd master_outdir/config_prefix1/map1.mrc/pdb_file_name1.pdb/
    ```

1. Calculate the statistics:

    ```
    Rscript <path_to_efitter>/efitter/scripts/pval_from_solutions.R solutions.csv cam_score
    ```

    You should get `solutions_pvalues.csv` file, which contains additional values for Z-scores and p-values. We usually use 'BH_adjusted_pvalues', which assess the p-value after multiple testing correction (Benjamini-Hochberg).

1. Optionally, to draw a bit nicer looking histogram of scores:

    ```
    python <path_to_efitter>/efitter/scripts/draw_solutions_histogram.py solutions.csv histogram.png
    ```

    Note: you need to have Python matplotlib installed for this script.

1. Control that all is fine with the background distribution and statistics by analyzing Rplots.pdf and optionally, histogram.png


### Visualization of the fits

#### Method 1
1. Enter the results directory for the given map:

    ```
    cd master_outdir/config_prefix1/map1.mrc/
    ```
1. Generate PDBs for multiple structures and/or maps into a single directory:
    ```
    cd fit_cam_inside0.3_fa_10000
    python <path_to_efitter>/efitter/scripts/genPDBs_many.py [options] outdir <solutions_list>
    ```
    e.g.:
    * Generate fits for all maps:
    Enter the "parameters set" fit directory like search100000_metric_cam_inside0.3_radius500/ and run:
    ```
    python <path_to_efitter>/efitter/scripts/genPDBs_many.py -n5 top5 */*/solutions.csv
    ```
    this will generate a director top5 with subdirectories for each map, and top 5 fits for each map.

    * Generate fits for a specific map:
    Enter the directory for specific map like search100000_metric_cam_inside0.3_radius500/P_negstain_01.mrc and run:
    ```
    python <path_to_efitter>/efitter/scripts/genPDBs_many.py -n5 top5 */solutions.csv
    ```
    this will generate a director top5 with top 5 fits for each map

#### Method 2

A bash command like this would iterate through all output directories and generate 10 fits there:

```
for f in `ls fits_chimera/fit_cam_inside0.3_Big/nr_8_norm_m3i_filt.no_membrane.mrc/* | grep ":" | perl -p -e "s/\://"`;
    do
        cd $f;
        genPDBs.py -n 10 solutions.csv ori_pdb.pdb
        cd /g/kosinski/kosinski/NPC/fitting/Chlamy
    done
```

#### Method 3 ("Manually")

1. Enter the directory for the given run:

    ```
    cd master_outdir/config_prefix1/map1.mrc/pdb_file_name1.pdb/
    ```

1. Generate the PDBs for a specified number of top fits

    ```
    python <path_to_efitter>/efitter/scripts/genPDBs.py -n 5 solutions.csv <path to the pdb files>/pdb_file_name1.pdb
    ```

1. Visualize the pdbs with your map in Chimera


# Example

In `efitter/test/` you can find an example fully-configured fitting job for the Elongator complex, (http://embor.embopress.org/content/early/2016/12/14/embr.201643353),
including the output.

# Visualization of results

## Examples:

### In situ structural analysis of the human nuclear pore complex
Extended data 7:
https://www.nature.com/articles/nature15381

### In situ architecture of the algal nuclear pore complex
Supplementary Figures 7-9:
https://www.nature.com/articles/s41467-018-04739-y

### Architecture of the yeast Elongator complex
Figure Expanded View 2, Figure Expanded View 4:
http://embor.embopress.org/content/18/2/264.long

## Drawing p-values plots:
To draw p-value plots I copy this script:
   ```efitter/scripts/plot_pvalues.py```
and modify according to the needs of the current figure. Let me know if you need any explanations.




