#from distutils.core import setup
from setuptools import setup

setup(
    name='efitter',
    version='0.9dev',
    author='Jan Kosinski',
    author_email='jan.kosinski@embl.de',
    packages=['efitter'
              ],
    scripts=[
        # 'scripts/fit_scop.py',
        # 'scripts/find_hits_colores.py',
        # 'scripts/fit_map_to_map_with_chimera.py',
        # 'scripts/rescore_tempy.py',
        # 'scripts/rotate.py',
        # 'scripts/score_with_TEMPy.py',
        # 'scripts/score_with_TEMPy_parallel.py',
        'scripts/genPDBs.py',
        'scripts/genPDBs_many.py',
        'scripts/fit_with_chimera_many.py',
        'scripts/fit_with_chimera.py',
        # 'scripts/plot_pvalues.py',
        # 'scripts/print_significant.py',
        # 'scripts/sort.py',
        # 'scripts/score_with_TEMPy_from_PDBs_parallel.py',
        # 'scripts/gen_molmap.py',
        'scripts/fit.py',
        'scripts/draw_solutions_histogram.py',

        'scripts/pval_from_solutions.R',
        'scripts/genpval.py',
        'scripts/fit_with_chimera',

             ],
    url='none',
    license='LICENSE.txt',
    description='Pipeline for fitting to EM maps using various fitting software (UCSF Chimera, PowerFit, ..., or... currently only Chimera supported ;-)',
    long_description=open('README.md').read(),
)
