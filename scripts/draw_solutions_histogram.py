#!/usr/bin/python
from __future__ import print_function
import csv
import sys
import math

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pylab


def read_solutions(solutions_filename, maxN=None):

    with open(solutions_filename, 'rU') as csvfile:
        dialect = csv.Sniffer().sniff(csvfile.readline(), [','])
        csvfile.seek(0)

        solutions = csv.DictReader(csvfile, dialect=dialect)

        for row in solutions:
            yield row


def main(solutions_filename, outfilename, score='cam_score', color_red_cutoff=None):
    scores = [float(sol[score]) for sol in read_solutions(solutions_filename)]
    fig = plt.figure()
    ax = fig.add_subplot(111)

    binsize = 0.1
    # maxVal = max(scores)
    # minVal = min(scores)
    # bins = range(0, int(math.ceil(maxVal)), binsize)
    # bins = range(0, 1, binsize)
    # bins = []
    # start = 0
    # for 

    # bins = bins[1:] #strip 0
    # bins.append(bins[-1]+0.1)


    # colors = []
    # for bin in bins:
    #     print(bin)
        # if bin >= xlinkanalyzer.XLINK_LEN_THRESHOLD:
        #     colors.append('#CC0000')
        # else:
        #     colors.append('#348ABD') #blue
        # print(bin, colors[-1])

    # ax.set_xbound(lower=min(scores))
    scores.sort(reverse=True)
    if score == 'overlap':
        max_score = max(scores)
    else:
        max_score = 1
    counts, bins, patches = ax.hist(scores, bins=100,range = ([min(scores),max_score])) #, rwidth=.5)
    if color_red_cutoff is not None:
        color_red_cutoff = float(color_red_cutoff)
        for bin, p in zip(bins, patches):
            if bin >= color_red_cutoff:
                p.set_color('#FF0000')
    # ticks = map(int, bins)

    # labels = []
    # for b in bins:
    #     if b >= min(scores):
    #         labels.append(b)
    # ax.set_xticklabels(labels, rotation=90, minor=True)#, ha='center')

    plt.xticks(bins[0::10], rotation=90, ha='left')

    Y_LABEL = "Number of fits"
    # X_LABEL = "Normalized cross-correlation"
    # X_LABEL = "Combined\nlocal cross-correlation and overlap score"
    X_LABEL = "Fitting score"
    ax.set_ylabel(Y_LABEL, fontsize=20)
    ax.set_xlabel(X_LABEL, fontsize=20)
    ax.tick_params(axis='both', which='major', labelsize=10)

    ax.annotate('{:.3g}'.format(scores[0]), xy=(scores[0], list(filter(lambda num: num != 0, counts))[-1]), xytext=(scores[0], max(counts)/2), fontsize=20, arrowprops=dict(facecolor='black', shrink=0.05))
    plt.tight_layout()
    # ax.yaxis.grid(True)
    ax.set_xlim(min(scores)*0.95,max_score*1.05)
    plt.savefig(outfilename)

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Usage: {0} solutions_filename outfilename [score] [color_red_cutoff]".format(sys.argv[0]))
        sys.exit()
    main(*sys.argv[1:])
