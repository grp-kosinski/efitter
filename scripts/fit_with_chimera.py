#!/usr/bin/python
'''
Usually this is run behind the scenes by fit.py but if you really need to run it:

chimera --pypath /g/kosinski/kosinski/devel/efitter/ --nogui --script "fit_with_chimera.py --move_to_center Nup93.pdb chimerafits/Nup93 fit_Nup93.txt"

chimera --pypath /g/kosinski/kosinski/devel/efitter/ --nogui --script "fit_with_chimera.py --ref ref_models/Nup93.pdb Nup93.pdb chimerafits/Nup93 fit_Nup93.txt"

chimera --pypath /g/kosinski/kosinski/devel/efitter/ --nogui --script "/g/kosinski/kosinski/devel/efitter/scripts/fit_with_chimera.py --move_to_center PDBs/Elp2.pdb test1 fits_chimera/test/emd_4151.map/config.txt"

or
chimera --script "fit_with_chimera.py --chimerafitmap_path /g/kosinski/kosinski/software/envs/assemblyLineTest/lib/python3.8/site-packages/efitter/chimerafitmap.py --move_to_center Nup93.pdb chimerafits/Nup93 fit_Nup93.txt"
and so on as above
'''
import os.path
import sys
import imp

# from efitter import chimerafitmap


import getopt
try:
    opts, args = getopt.getopt(sys.argv[1:], '', ['ref=', 'move_to_center', 'backbone_only', 'chimerafitmap_path='])
except getopt.error, message:
    raise chimera.NonChimeraError("%s: %s" % (__name__, message))

ref_model = None
move_to_center = False
backbone_only = False
for o in opts:
    if o[0] in ("--ref"):
        ref_model = o[1]
    if o[0] in ("--move_to_center"):
        move_to_center = True
    if o[0] in ("--backbone_only"):
        backbone_only = True
    if o[0] in ("--chimerafitmap_path"):
        chimerafitmap_path = o[1]

chimerafitmap = imp.load_source('chimerafitmap', chimerafitmap_path)

model_filename = args[0]
out_dir = args[1]
config_filename = args[2]

print('model_filename', model_filename)
print('out_dir', out_dir)
print('ref_model', ref_model)
print('config_filename', config_filename)

r = chimerafitmap.Runner()
solutions_filename = os.path.join(out_dir, 'solutions.csv')
r.backbone_only = backbone_only
cfg = r.make_config(config_filename)
if ref_model is not None:
    r.open_ref_model(ref_model)
r.open_map()
r.open_query(model_filename)
if move_to_center:
    r.move_to_center()
r.run(model_filename, out_dir)
r.write_solutions(solutions_filename)
