#!/usr/bin/python
from optparse import OptionParser
import csv
import os

from pdb_utils import transform_pdb

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i+n]

def read_solutions(solutions_filename, maxN=None):

    with open(solutions_filename, 'rU') as csvfile:
        dialect = csv.Sniffer().sniff(csvfile.readline(), [','])
        csvfile.seek(0)

        solutions = csv.DictReader(csvfile, dialect=dialect)

        for i, row in enumerate(solutions):
            if maxN is None or (maxN is not None and i < maxN):
                row['rotation'] = list(map(float, row['rotation'].split()))
                row['translation'] = list(map(float, row['translation'].split()))

                yield row

def genPDBs(solutions_fn, pdb_fn, outdir=os.getcwd(), no_of_hits=None, sol_id=None):
    if sol_id is not None:
        no_of_hits = None
    for sol in read_solutions(solutions_fn, maxN=no_of_hits):
        if sol_id is not None and int(sol['solution_id']) != sol_id:
            continue
        print(sol)
        rot = list(chunks(sol['rotation'], 3))
        newrot = [
            [],
            [],
            []
        ]
        for row in rot:
            for i in range(3):
                newrot[i].append(row[i])
        rot = newrot

        trans = sol['translation']


        pdblines = transform_pdb.transform(pdb_fn, rot, trans)

        outpdb_fn = os.path.join(outdir, sol['filename'])

        with open(outpdb_fn, 'w') as outpdb_f:
            outpdb_f.write(''.join(pdblines))

def calc_center(coords):
    '''Equivalent of situs calc_center'''
    cx = cy = cz = 0.0

    for x, y, z in coords:
        cx += x
        cy += y
        cz += z
    coords_len = len(coords)
    return cx/coords_len, cy/coords_len, cz/coords_len

def read_coords(pdbfilename):
    coords = []

    with open(pdbfilename, 'rU') as f:
        for line in f:
            if line[0:6] in ('ATOM  ', 'HETATM'):
                x = float(line[30:38])
                y = float(line[38:46])
                z = float(line[46:54])
                coords.append((x, y, z))

    return coords

def center(coords):
    ori_center = calc_center(coords)
    return transform_pdb.transform_coordinates(coords, rot=[[1, 0, 0], [0, 1, 0], [0, 0, 1]], trans=[-x for x in ori_center])

def write_pdb(coords, ori_pdbfilename, new_pdbfilename):
    i = 0
    outlines = []
    with open(ori_pdbfilename, 'rU') as f:
        for line in f:
            if line[0:6] in ('ATOM  ', 'HETATM'):
                x, y, z = coords[i]
                new_x = '%8.3f' % (x)
                new_y = '%8.3f' % (y)
                new_z = '%8.3f' % (z)
                line = line[:30] + new_x + new_y + new_z + line[54:]
                i = i + 1

            outlines.append(line)

    with open(new_pdbfilename, 'w') as f:
        f.write(''.join(outlines))

def genPDBs_colores(solutions_fn, pdb_fn, outdir=os.getcwd(), no_of_hits=None):

    ori_coords = read_coords(pdb_fn)
    ori_center = calc_center(ori_coords)
    #colores first moves fit component to center of coord system
    ini_rot = [[1,0,0],[0,1,0],[0,0,1]]
    ini_trans = [-x for x in ori_center]

    with open(solutions_fn) as fh:
        next(fh)
        next(fh)
        for line in fh:
            data = list(map(float, line.split()))
            print(data)
            euler_rot = data[1:4]
            rot_center = data[4:7]
            ccc = data[7]
            print(euler_rot, rot_center, ccc)
            sol_rot = transform_pdb.euler_to_matrix(*euler_rot, in_degrees=True)
            sol_trans = rot_center

            coords = center(ori_coords)

            coords = transform_pdb.transform_coordinates(coords, rot=sol_rot, trans=sol_trans)

            

            outpdb_fn = 'sol_{0}.pdb'.format(int(data[0]))
            outpdb_fn = os.path.join(outdir, outpdb_fn)

            write_pdb(coords, pdb_fn, outpdb_fn)

def main():
    usage = "usage: %prog [options] solutions.csv ori_pdb"
    parser = OptionParser(usage=usage)

    parser.add_option("-n", type="int", dest="no_of_hits", default=None,
                      help="Number of hits [default: %default]")

    parser.add_option("-i", type="int", dest="sol_id", default=None,
                      help="Id of a specific hit, count from 0 [default: %default]")

    parser.add_option("-o", "--outdir", dest="outdir", default=os.getcwd(),
                      help="optional DIR with output", metavar="DIR")

    parser.add_option("--colores", dest="colores", action="store_false", default=False,
                      help="input is colores?")

    (options, args) = parser.parse_args()

    solutions_fn = args[0]
    pdb_fn = args[1]

    if not options.colores:
        genPDBs(solutions_fn, pdb_fn, options.outdir, options.no_of_hits, options.sol_id)
    else:
        genPDBs_colores(solutions_fn, pdb_fn, options.outdir, options.no_of_hits)

if __name__ == '__main__':
    main()
