args = commandArgs(trailingOnly=T)
blast_tab_file=args[1]
output_fname=args[2]

#never ever change these, same in all of the pipeline
seq1id="QueryId"
seq2id="SubjectId"
seq1start="q.start"
seq1stop="q.end"
seq2start="s.start"
seq2stop="s.end"
eval="e-value"
gaps="gapOpenings"
seq1="q.seq"
seq2="s.seq"
threshold=1e-40

blast_data=read.table(blast_tab_file,sep=",",header=T,check.names=F)
write(paste(seq1id,seq2id,seq1start,seq1stop,seq2start,seq2stop,eval,gaps,seq1,seq2,sep=","),file=output_fname)
for (l in levels(blast_data[,seq1id])){
	subframe=blast_data[blast_data[,seq1id]==l,]
	minrows=subframe[as.numeric(as.character(subframe[,eval]))<threshold,]
	row=paste(minrows[,seq1id],minrows[,seq2id],minrows[,seq1start],minrows[,seq1stop],minrows[,seq2start],minrows[,seq2stop],minrows[,eval],minrows[,gaps],minrows[,seq1],minrows[,seq2],sep=",")
	write(row,file=output_fname,append=T)
}
