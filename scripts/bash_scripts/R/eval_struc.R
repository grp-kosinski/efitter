args = commandArgs(trailingOnly=TRUE)
fitting_dir=args[1]
pdbfile_name=args[2]
pval_id=args[3]
score_id=args[4]
max_n=as.numeric(args[5])
order_by_score=as.numeric(args[6])
tmp_file=args[7]
order_decrease=F

order_by=pval_id
if (as.logical(order_by_score)){
	order_by=score_id
	order_decrease=T
} 
pval_threshold=0.1
sol_id="solution_id"

PVAL_FILE_NAME="solutions_pvalues.csv"
PRINT_COLS=30
START_COLS=15

#script makes sure that pvalue files exist
pdb_dirs=list.files(fitting_dir,recursive=T,full.names=T,pattern=pdbfile_name,include.dirs=T)
cat(sprintf("\n\t\t\t+++ Overview for %20s +++\n\n",pdbfile_name))
letter_count=1
for (pdb_dir in pdb_dirs){
	if (file.exists(paste(pdb_dir,"/solutions_pvalues.csv",sep="")){
		file_data=read.table(paste(pdb_dir,"/solutions_pvalues.csv",sep=""),header=T,sep=",")
		print_data=file_data[order(file_data[,order_by]),c(pval_id,score_id,sol_id)]
		pdb_name = basename(pdb_dir)
        	mrc_dir = dirname(pdb_dir)
        	mrc_name = basename(mrc_dir)
		write(sprintf("%s\t%s",LETTERS[letter_count],mrc_name),file=tmp_file,append=T)
		if (nchar(pdb_name) > PRINT_COLS){
        		pdb_name = paste(substr(pdb_name,1,START_COLS),"...",substr(pdb_name,nchar(pdb_name)-9,nchar(pdb_name)),sep="")
        	}

        	if (nchar(mrc_name) > PRINT_COLS){
        		mrc_name = paste(substr(mrc_name,1,START_COLS),"...",substr(mrc_name,nchar(mrc_name)-11,nchar(mrc_name)),sep="")
        	}
        	head_string = sprintf("%5s\t%30s\t%30s\t%10s\t%10s\t%10s\n",paste("[",LETTERS[letter_count],"]",sep=""),mrc_name,pdb_name,paste(substr(pval_id,0,7),"...",sep=""),score_id,paste(substr(sol_id,0,4),"...",sep=""))
		cat(head_string)
		for (i in 1:min(max_n,nrow(print_data))){
			print_string=sprintf("%5s\t%30s\t%30s\t%10s\t%10f\t%4i\n","","","",formatC(print_data[i,pval_id],format="e",digits=1),print_data[i,score_id],print_data[i,sol_id])
			cat(print_string)
		}
		letter_count=letter_count+1
	}
}

