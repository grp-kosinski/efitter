#!/bin/bash
module load R
module load matplotlib/2.1.2-foss-2017b-Python-2.7.14;
DIRECTORY=$(cd `dirname $0` && pwd)
PVALUE_SCRIPT=$DIRECTORY/R/pval_from_solutions.R;
HISTOGRAM_SCRIPT=$DIRECTORY/python/draw_solutions_histogram.py;
SOLUTIONS_FN="solutions.csv"
SOLUTIONS_PVAL_FN="solutions_pvalues.csv"

usage() {
    echo `basename $0`: ERROR: $* 1>&2
    echo usage: `basename $0` ' [ fitting directory ] [ -p print statistics ] ' 1>&2
    exit 1
}
#set option and check variables
base_dir=`readlink -f $1`;shift;
if [[ ! -d $base_dir ]];then
    usage "No such directory: $base_dir"
fi
print_stats=false;

#handle optional arguments, follow the conventions
while :
do
    case "$1" in
        -p) shift; print_stats=true;;
        --) shift; break;;
        -*) usage "bad argument $1";;
        *) break;;
    esac
done
solutions_files=`find $base_dir -type f -name $SOLUTIONS_FN`;
for file in $solutions_files;
do
	number_of_solutions=`wc -l < "${file}"`;
	echo "Evaluating ${file} with ${number_of_solutions} solution.";
	if [ $number_of_solutions -le 1 ];
	then
		usage "File ${file} has one or less solutions."
	fi
	dir_name=`dirname "${file}"`;
    	Rscript $PVALUE_SCRIPT "${file}" cam_score "${dir_name}"/"${SOLUTIONS_PVAL_FN}";
	if [ "${print_stats}" = true ];
	then
		python $HISTOGRAM_SCRIPT "${file}" "${dir_name}"/histogram.png;
	fi
done

