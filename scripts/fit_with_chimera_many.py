#!/usr/bin/python
'''
chimera --pypath /g/kosinski/kosinski/devel/efitter/ --nogui --script "fit_with_chimera_many.py pdb_file_list.txt out_dir cfg_file"
e.g.:

chimera --pypath /g/kosinski/kosinski/devel/efitter/ --nogui --script "fit_with_chimera.py --move_to_center pdb_file_list.txt chimerafits/ fit_Nup93.txt"

chimera --pypath /g/kosinski/kosinski/devel/efitter/ --nogui --script "fit_with_chimera.py --ref ref_models/Nup93.pdb pdb_file_list.txt chimerafits/ fit_Nup93.txt"

'''
import os.path
import sys

from efitter import chimerafitmap

import getopt
try:
    opts, args = getopt.getopt(sys.argv[1:], '', ['ref=', 'move_to_center', 'backbone_only'])
except getopt.error, message:
    raise chimera.NonChimeraError("%s: %s" % (__name__, message))



ref_model = None
move_to_center = False
backbone_only = False
for o in opts:
    if o[0] in ("--ref"):
        ref_model = o[1]
    if o[0] in ("--move_to_center"):
        move_to_center = True
    if o[0] in ("--backbone_only"):
        backbone_only = True

models_list_filename = args[0]
out_dir = args[1]
config_filename = args[2]

print('models_list_filename', models_list_filename)
print('out_dir', out_dir)
print('ref_model', ref_model)
print('config_filename', config_filename)


r = chimerafitmap.Runner()
solutions_filename = os.path.join(out_dir, 'solutions.csv')
r.backbone_only = backbone_only
cfg = r.make_config(config_filename)
if ref_model is not None:
    r.open_ref_model(ref_model)
r.open_map()
with open(models_list_filename) as f:
    for line in f:
        model_filename = line.strip('\n')
        r.open_query(model_filename)
        if move_to_center:
            r.move_to_center()
        r.run(model_filename, out_dir)
        r.close_model()

r.write_solutions(solutions_filename)
