import sys
import subprocess
import glob
import os
import ntpath
import itertools
from string import Template
import os.path
import tempfile

class Map(object):
    def __init__(self, filename, threshold = None, resolution = None):
        self.filename = filename
        self.threshold = threshold
        self.resolution = resolution

    def __str__(self):
        return ntpath.basename(self.filename)

    def __repr__(self):
        return 'Map({0})'.format(ntpath.basename(self.filename))

master_outdir='fits_SCOP_1.25'
ref_models_dir = ''
models_dir = '/g/scb/cmueller/kosinski/db/SCOPe40/pdbstyle-2.06'
method = 'powerfit' #choices: 'chimera', 'powerfit'
CA_only = True


MAPS = [

    Map('/g/scb/cmueller/kosinski/PolI/fitting/fit_helical/maps/helical_densities.mrc', threshold=0.005, resolution=5.0),

]

fitmap_args = [
    {
        'template': Template(""""""),
        'config_prefix': 'default'
    },

]

used = []
for cfg in fitmap_args:
    if cfg['config_prefix'] in used:
        print "Duplicated config prefiex in fitmap_args, duplicate is: {0}".format(cfg['config_prefix'])
        sys.exit()
    used.append(cfg['config_prefix'])


with open('/g/scb/cmueller/kosinski/PolI/fitting/fit_helical/scope_a.118.8.1_list.txt') as f:
    temp = map(lambda s: s[2:4]+'/'+s+'.ent', [line.split()[0] for line in f])

    PDB_FILES = []
    for f in temp:
        if os.path.exists(os.path.join(models_dir,f)):
            PDB_FILES.append(f)
        else:
            print '{0} does not exists'.format(f)
print PDB_FILES

combs = []
for combination in itertools.product(MAPS, fitmap_args, PDB_FILES):
    combs.append(
        {
            'map': combination[0],
            'fitmap_args': combination[1],
            'pdb_file': combination[2]
        }
        )
print "Will output to {0} using {1}".format(master_outdir, method)
var = raw_input("The script will now run {0} jobs, proceed? [y/n]?: ".format(len(combs)))
if var != 'y':
    print "Exiting"
    sys.exit()
print len(combs)

# if CA_only:
#     models_dir = tempfile.mkdtemp()
#     for f in PDB_FILES:
#         path = os.path.join(models_dir,f)
#         outf = os.path.join(models_dir, f)
#         subprocess.call("egrep '.{13}CA' {0} > {1}".format(f, outf), shell=True)

for comb in combs:
    outdir = os.path.join(master_outdir, comb['fitmap_args']['config_prefix'], str(comb['map']))
    # print outdir
    subprocess.call('mkdir -p {0}'.format(outdir), shell=True)

    if method == 'chimera':
        config_fn = os.path.join(
            outdir,
            'config.txt'
            # '{0}_{1}.txt'.format(comb['fitmap_args']['config_prefix'], str(comb['map']))
            )
        config_str = comb['fitmap_args']['template'].substitute(
                threshold=comb['map'].threshold,
                map=comb['map'].filename,
                resolution=comb['map'].resolution
            )

        with open(config_fn, 'w') as f:
            f.write(config_str)

    pdb_outdir = os.path.join(outdir, comb['pdb_file'])
    # print pdb_outdir
    subprocess.call('rm -rv {0}'.format(pdb_outdir), shell=True)
    subprocess.call('mkdir -p {0}'.format(pdb_outdir), shell=True)


    # print ref_models_dir
    ref_pdb_file = comb['pdb_file'].replace('.Calpha','')
    job_name = '_'.join([comb['fitmap_args']['config_prefix'], str(comb['map']),comb['pdb_file']])
    # print job_name



    if method == 'chimera':
        cmd = 'bsub -q medium_priority -M 15000 -R"select[(mem > 16000)]" -n 1 -o {pdb_outdir}/bsub.out -J {job_name} "fit_with_chimera --ref {ref_models_dir}/{ref_pdb_file} {models_dir}/{pdb_file} {pdb_outdir} {config} &> {pdb_outdir}/log.txt"'.format(
            pdb_file=comb['pdb_file'],
            pdb_outdir=pdb_outdir,
            config=config_fn,
            models_dir=models_dir,
            ref_models_dir=ref_models_dir,
            job_name=job_name,
            ref_pdb_file=ref_pdb_file
            )
    elif method == 'powerfit':
        if CA_only:
            cmd = 'bsub -q medium_priority -M 15000 -R"select[(mem > 16000)]" -n 1 -o {pdb_outdir}/bsub.out -J {job_name} "egrep \'.{{13}}CA\' {models_dir}/{pdb_file} > /tmp/{tempfile}; powerfit {map} {resolution} /tmp/{tempfile} -d {pdb_outdir} -n 10 -p 1 &> {pdb_outdir}/log.txt; rm /tmp/{tempfile}"'.format(
                pdb_file=comb['pdb_file'],
                pdb_outdir=pdb_outdir,
                models_dir=models_dir,
                job_name=job_name,
                map=comb['map'].filename,
                resolution=comb['map'].resolution,
                tempfile=os.path.basename(comb['pdb_file'])
                )

        else:
            cmd = 'bsub -q medium_priority -M 15000 -R"select[(mem > 16000)]" -n 1 -o {pdb_outdir}/bsub.out -J {job_name} "powerfit {map} {resolution} {models_dir}/{pdb_file} -d {pdb_outdir} -n 10 -p 1 &> {pdb_outdir}/log.txt"'.format(
                pdb_file=comb['pdb_file'],
                pdb_outdir=pdb_outdir,
                models_dir=models_dir,
                job_name=job_name,
                map=comb['map'].filename,
                resolution=comb['map'].resolution
                )

    print cmd

    subprocess.call(cmd, shell=True)
