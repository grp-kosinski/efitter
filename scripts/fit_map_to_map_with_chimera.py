'''
chimera --pypath /g/kosinski/kosinski/devel/efitter/ --nogui --script "fit_map_to_map_with_chimera.py query.mrc out_dir query_map_threshold cfg_file"
'''
import os.path

import chimerafitmap

query_filename = arguments[0]
out_dir = arguments[1]
query_map_threshold = arguments[2]
config_filename = arguments[3]

r = chimerafitmap.Runner()
solutions_filename = os.path.join(out_dir, 'solutions.csv')
cfg = r.make_config(config_filename)
r.open_map()
r.open_query(query_filename)
r.set_query_map_level(float(query_map_threshold))
r.run(query_filename, out_dir)
r.write_solutions(solutions_filename)
