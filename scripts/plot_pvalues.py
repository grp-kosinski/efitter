import sys
import math

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pylab


from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes, inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

NON_SIGNIFICANT_COLOR = '#00cccc'
SIGNIFICANT_COLOR = 'red'
SIGNIFICANT_COLOR_2 = '#00CC00'
SIGNIFICANT_COLOR_3 = 'blue'


Y_LABEL = "Adjusted p-value"
X_LABEL = "solution id"
import matplotlib as mpl
mpl.rcParams['lines.linewidth'] = 20



def main(solutions_filename, outfilename):
    pvals = []
    with open(solutions_filename) as f:
        next(f)
        for line in f:
            pvals.append(float(line.strip().split(',')[-1]))
            # print(pvals[-1])


    fig, ax = plt.subplots(figsize=(20, 10))


    sign_pval = 0.000001
    sign_pval_idx = None
    for i, pval in enumerate(pvals):
        if pval > sign_pval:
            sign_pval_idx = i - 1
            break


    # ax.set_xlim(-len(pvals)/4, len(pvals)+len(pvals)/4)
    # colors = [SIGNIFICANT_COLOR] * 1 + [SIGNIFICANT_COLOR_2] * 2 + [SIGNIFICANT_COLOR_3] * 17 + [NON_SIGNIFICANT_COLOR] * (len(pvals) - sign_pval_idx+1)
    # colors = [SIGNIFICANT_COLOR] * 5 + [SIGNIFICANT_COLOR_3] * 2 + [NON_SIGNIFICANT_COLOR] * (len(pvals) - 7)
    # colors = [NON_SIGNIFICANT_COLOR] * (len(pvals))
    colors = [SIGNIFICANT_COLOR] * (sign_pval_idx+1) + [NON_SIGNIFICANT_COLOR] * (len(pvals) - sign_pval_idx+1)
    # ax.scatter(range(1, len(pvals)+1), sorted(pvals), [100.0 for i in pvals], c=colors, marker=u'o', edgecolors='none')

    #draw the significant points again so they are in front
    # ax.scatter(range(1, 7), sorted(pvals[:6]), [100.0 for i in pvals], c=colors, marker=u'o', edgecolors='none')

    # axins = inset_axes(ax, width="50%", height="50%", loc=7)
    n_points = 5
    
    ax.scatter(range(1, len(pvals[:n_points])+1), sorted(pvals[:n_points]), [200.0 for i in pvals[:n_points]], c=colors, marker=u'o', edgecolors='none')
    ax.axis([-0.5, n_points+1, -0.00001, pvals[n_points+1]+pvals[n_points+1]/5])

    for i in range(n_points):
        ax.annotate('{:.2g}'.format(pvals[i]), xy=(i+1, pvals[i]), xytext=(-10, 10), textcoords='offset points', fontsize=40)#, arrowprops=dict(facecolor='black', shrink=0.05))
        ax.annotate('{:.2g}'.format(pvals[i]), xy=(i+1, pvals[i]), xytext=(-10, 10), textcoords='offset points', fontsize=40)#, arrowprops=dict(facecolor='black', shrink=0.05))
        ax.annotate('{:.2g}'.format(pvals[i]), xy=(i+1, pvals[i]), xytext=(-10, 10), textcoords='offset points', fontsize=40)#, arrowprops=dict(facecolor='black', shrink=0.05))

    #STYLE:
    # ax.set_ylabel(Y_LABEL, fontsize=40)
    # ax.set_xlabel(X_LABEL, fontsize=40)
    # # ax.set_xlabel(r"Euclidan C$\alpha$ pair distance [$\AA$]")
    # ax.yaxis.grid(True)
    # ax.yaxis.grid(True)
    # ax.tick_params(axis='both', which='major', labelsize=30)
    # ax.set_axis_bgcolor('white')
    # for axis in ['top','bottom','left','right']:
    #     ax.spines[axis].set_linewidth(3)

    ax.set_ylabel(Y_LABEL, fontsize=60)
    ax.set_xlabel(X_LABEL, fontsize=60)
    # ax.set_xlabel(r"Euclidan C$\alpha$ pair distance [$\AA$]")
    ax.yaxis.grid(False)
    ax.yaxis.grid(False)

    ax.tick_params(axis='both', which='major', labelsize=40, width=6, length=15)
    ax.set_facecolor('white')
    for axis in ['top','bottom','left','right']:
        ax.spines[axis].set_linewidth(6)

    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')

    xticks = ax.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    # print(dir(xticks[0].label1))
    # xticks[1].label1.set_text('1')
    a = ax.get_xticks().tolist()
    a = list(map(int, a))
    a[1] ='1'
    ax.set_xticklabels(a)

    # axins.set_ylabel(Y_LABEL, fontsize=35)
    # axins.set_xlabel(X_LABEL, fontsize=35)
    # axins.tick_params(axis='both', which='major', labelsize=30)
    # axins.yaxis.grid(False)
    # axins.yaxis.grid(False)    
    # for axis in ['top','bottom','left','right']:
    #     axins.spines[axis].set_linewidth(2)
    # mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5", linewidth=2)
    # xticks = axins.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    xticks[1].label1.set_visible(False)

    # for axis in ['top','bottom','left','right']:
    #     axins.spines[axis].set_linewidth(3)

    plt.tight_layout()
    plt.savefig(outfilename, transparent=True)

if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])