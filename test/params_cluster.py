'''
Automatically fits several structures against several maps,
and with alternative parameters.
Automatically submits to the HD cluster.
'''

from efitter import Map
from string import Template

method='chimera'
dry_run = False # Dry run would only print commands it is going to run
run_crashed_only = False # Only run the jobs that have not delivered output
master_outdir = 'fits_chimera/' 

MAPS = [
    Map('EM/emd_4151.map', threshold=0.0203, resolution=25)

]

models_dir = 'PDBs'

PDB_FILES = [
    'Elp1.CTD.on5cqs.5cqr.model.pdb',
    'Elp2.pdb',
    'Elp3.mono.pdb'
]

CA_only = False # Calculate the fitting scores using Calpha atoms only?
backbone_only = False # Calculate the fitting scores using backbone atoms only?
move_to_center = True # Move the PDB structure to the center of the map?

# Each element of fitmap_args is a dictionary specifying parameters for a run
# If multiple dictionaries are specified,
# the script will run a seperate run for each dictionary for each map-structure combination.
# E.g. if two maps, three structures, and two paramaters dictionaries are specified,
# the script will run 2 x 3 x 2 = 12 runs.
fitmap_args = [
    # We suggest to run a small test run with search 100 first
    {
        'template': Template("""
            map $map
            map_threshold $threshold
            fitmap_args resolution $resolution metric cam envelope true search 100 placement sr clusterAngle 3 clusterShift 3.0 radius 200 inside .30
            saveFiles False
            """),
        'config_prefix': 'test' # some name informative of the fitmap_args parameters
        },
    {
        'template': Template("""
            map $map
            map_threshold $threshold
            fitmap_args resolution $resolution metric overlap envelope true search 100 placement sr clusterAngle 3 clusterShift 3.0 radius 200 inside .30
            saveFiles False
            """),
        'config_prefix': 'test_overlap' # some name informative of the fitmap_args parameters
        },
    # Paramters for main runs (https://www.cgl.ucsf.edu/chimera/docs/UsersGuide/midas/fitmap.html)
    # {
    #     'template': Template("""
    #         map $map
    #         map_threshold $threshold
    #         fitmap_args resolution $resolution metric cam envelope true search 100000 placement sr clusterAngle 3 clusterShift 3.0 radius 200 inside .30
    #         saveFiles False
    #         """),
    #     'config_prefix': 'search100000_metric_cam_inside0.3' # some name informative of the fitmap_args parameters
    #     }
]

# If necessary, edit the below template following specifications of
# string.Template and Slurm sbatch 
cluster_submission_command = 'sbatch'
run_script_templ = Template("""#!/bin/bash
#
#SBATCH --job-name=$job_name
#SBATCH --time=01:00:00
#SBATCH --error $pdb_outdir/log_err.txt
#SBATCH --output $pdb_outdir/log_out.txt
#SBATCH --mem=1000

$cmd
""")
